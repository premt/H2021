using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class zombie : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        RagDoll(true);
    }


    // Ragdoll info

    void RagDoll(bool value)
    {
        var bodyParts = GetComponentsInChildren<Rigidbody>();
        foreach(var bodyPart in bodyParts)
        {
            bodyPart.isKinematic = value;
        }

    }


    // Kill zombie info

    void KillZombie(RaycastHit hitLocationInfo)
    {
        GetComponent<Animator>().enabled = false;
        RagDoll(false);
        Vector3 hitPoint = hitLocationInfo.point;
        var colliders = Physics.OverlapSphere(hitPoint, 0.5f);
        foreach(var collider in colliders)
        {
            var rigidBody = collider.GetComponent<Rigidbody>();
            if (rigidBody)
            {
                rigidBody.AddExplosionForce(1000, hitPoint, 0.5f);

            }
           
        }
        this.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
